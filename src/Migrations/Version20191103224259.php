<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191103224259 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE events ADD city_id INT DEFAULT NULL, DROP city');
        $this->addSql('ALTER TABLE events ADD CONSTRAINT FK_5387574A8BAC62AF FOREIGN KEY (city_id) REFERENCES pays (id)');
        $this->addSql('CREATE INDEX IDX_5387574A8BAC62AF ON events (city_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE events DROP FOREIGN KEY FK_5387574A8BAC62AF');
        $this->addSql('DROP INDEX IDX_5387574A8BAC62AF ON events');
        $this->addSql('ALTER TABLE events ADD city INT NOT NULL, DROP city_id');
    }
}
