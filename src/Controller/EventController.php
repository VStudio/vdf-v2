<?php

namespace App\Controller;

use App\Entity\Events;
use App\Repository\EventsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

class EventController extends AbstractController
{
    /**
     * @Route("/events", name="event.public")
     * @return Response
     */
    public function index()
//    public function index(PaginatorInterface $paginator, EventsRepository $eventRepository, Request $request):Response
    {

//        $events = $paginator->paginate(
//            $eventRepository->getPublished(),
//            $request->query->getInt('page',1),12
//        );
//        dump($events);
        return $this->render('event/index.html.twig');
    }

//    /**
//     * @Route("/event/{slug}", name="event_details.public")
//     */
//    public function show(Events $event):Response
//    {
//        return $this->render('event/details.html.twig', compact('event'));
//    }
}
