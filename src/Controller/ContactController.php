<?php

namespace App\Controller;

use App\Entity\ContactMessage;
use App\Events;
use App\Form\ContactFormType;
use App\Services\GoogleRecaptcha;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ContactController extends AbstractController
{
//    /**
//     * @Route("/contact_us", name="contact.public")
//     * @param Request $request
//     * @param ObjectManager $manager
//     * @param EventDispatcherInterface $eventDispatcher
//     * @param TranslatorInterface $translator
//     * @param GoogleRecaptcha $recaptcha
//     * @return Response
//     * @throws \Exception
//     */
//    public function index(
//        Request $request,
//        ObjectManager $manager,
//        EventDispatcherInterface $eventDispatcher,
//        TranslatorInterface $translator,
//        GoogleRecaptcha $recaptcha
//    ): Response {
//        $contact = new ContactMessage();
//        $form = $this->createForm(ContactFormType::class, $contact);
//        $form->handleRequest($request);
//        if ($form->isSubmitted() && $form->isValid()) {
//            $gRecaptchaResponse = $recaptcha->verify($request->get('g-recaptcha-response'), $request->getClientIp());
//
//            if (!$gRecaptchaResponse->isSuccess()) {
//                $errors = $gRecaptchaResponse->getErrorCodes();
//                throw new \Exception('Score de validation recaptcha non atteint. ', 1);
//                $this->redirectToRoute('contact.public');
//            }
//            $contact->setIpAddress($request->getClientIp());
//            $manager->persist($contact);
//            $manager->flush();
//            $event = new GenericEvent($contact);
//            $eventDispatcher->dispatch($event, Events::CONTACT_FORM_SUBMIT);
//            $request->getSession()->getFlashBag()->add('info', $translator->trans('label.contact.created_successfully'));
//            $this->redirectToRoute('contact.public');
//        }
//
//        return $this->render('contact/index.html.twig', ['form' => $form->createView()]);
//    }
    /**
     * @Route("/contact_us", name="contact.public")
     */
    public function index(){
        return $this->render('contact/index.html.twig');
    }
}
