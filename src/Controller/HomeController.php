<?php
/**
 * Created by PhpStorm.
 * User: utilisateur
 * Date: 11/5/2019
 * Time: 5:54 AM
 */

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home.public")
     */
    public function index()
    {
        return $this->render('home/index.html.twig');
    }
}