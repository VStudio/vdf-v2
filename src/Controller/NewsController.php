<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface;
use App\Repository\NewsRepository;
use App\Entity\News;

class NewsController extends AbstractController
{
    /**
     * @Route("/news", name="news.public")
     */
//    public function index(PaginatorInterface $paginator, NewsRepository $newsRepository, Request $request):Response
    public function index()
    {
//        $datas = $paginator->paginate(
//            $newsRepository->getNewsActive(),
//            $request->query->getInt('page', 1),
//            12
//        );
        return $this->render('news/index.html.twig');
    }

//    /**
//     * @Route("/news/{slug}", name="news_details.public")
//     */
//    public function show(News $news)
//    {
//        return $this->render('news/details.html.twig', compact('news'));
//    }
}
