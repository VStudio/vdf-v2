<?php
/**
 * Created by PhpStorm.
 * User: utilisateur
 * Date: 11/3/2019
 * Time: 1:07 PM
 */
namespace App\EventListener;

use App\Entity\Person;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CreateUserListener
{
    private $container;
//protected $db;
    public function __construct(Container $container) {
        $this->container = $container;
    }
    // the entity listener methods receive two arguments:
    // the entity instance and the lifecycle event
    public function postPersist(Person $person, LifecycleEventArgs $event)
    {
        // ... do something to notify the changes
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $user->setUsername(substr($person->getFirstname(),0,3).substr($person->getLastname(),-1,3).substr(mt_rand(),0,4));
        $user->setUsernameCanonical(substr($person->getFirstname(),0,3).substr($person->getLastname(),-1,3).substr(mt_rand(),0,4));
        $user->setEmail($person->getEmail());
        $user->setEmailCanonical($person->getEmail());
        $user->setEnabled(true);
        $user->addRole('ROLE_ADMIN');
        $user->setPerson($person);
        // this method will encrypt the password with the default settings :)
        $user->setPlainPassword('123456');
        $userManager->updateUser($user);
    }
}